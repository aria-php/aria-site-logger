<?php

namespace ARIA\core\logging\Handler; 
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

use ARIA\GraphQLClient\Client;
use ARIA\GraphQLClient\API\SiteAPI;

class SiteLogHandler extends AbstractProcessingHandler {

    private $site_id = "";
    private $token = "";
    private $endpoint;


    public function __construct( string $site_id, string $endpoint, string $token, $level = Logger::DEBUG, bool $bubble = true ) {

        parent::__construct($level, $bubble);

        $this->site_id = $site_id;
        $this->endpoint = $endpoint;
        $this->token = $token;
    }

    protected function write(array $record): void
    {

        try {
            $api_client = new Client( $this->endpoint );
            $api_client->setToken( $this->token );
            
            $site_api = new SiteAPI( $api_client );

            $level = "";
            switch ($record['level']) {
                case Logger::DEBUG: $level = SiteAPI::SITE_LOG_LEVEL_DEBUG; break;
                case Logger::INFO: $level = SiteAPI::SITE_LOG_LEVEL_INFO; break;
                case Logger::WARNING: $level = SiteAPI::SITE_LOG_LEVEL_WARNING; break;
                case Logger::ERROR: $level = SiteAPI::SITE_LOG_LEVEL_ERROR; break;
                case Logger::CRITICAL: $level = SiteAPI::SITE_LOG_LEVEL_CRIT; break;
                default:
                    $level = $record['level'];
            }

            $site_api->log( $this->site_id, $record['message'], $level, $record['context']??[]);
        } catch (\Exception $e) {
            error_log('SiteLogHandler: ' . $e->getMessage());
        }
    }

}